<?php
include_once('Skier.php');
include_once('Club.php');
include_once('Season.php');

class readXML{
	
	private $xmlDoc;
	
	public function __construct(){
		$this->xmlDoc = new DOMDocument();
		$this->xmlDoc->load('SkierLogs.xml');
	}	
	
	public function getSkiers(){				//gjør akkurat det samme som getClubs bare med skier
		$skiersArray = array();
		$allSkiers = $this->xmlDoc->getElementsByTagName('Skiers')[0]->getElementsByTagName('Skier');
		
		foreach($allSkiers as $skier){
		$skiersArray[count($skiersArray)] = new Skier($skier->getAttribute('userName'), $skier->childNodes[1]->nodeValue, 
		$skier->childNodes[3]->nodeValue, $skier->childNodes[5]->nodeValue);
		}
		return $skiersArray;	
	}
	
	public function getClubs(){
		$clubsArray = array();
        $allClubs = $this->xmlDoc->getElementsByTagName('Clubs')[0]->getElementsByTagName('Club');
		
		foreach($allClubs as $club){
			$clubsArray[count($clubsArray)] = new Club($club->getAttribute('id'), $club->childNodes[1]->nodeValue,
				$club->childNodes[3]->nodeValue, $club->childNodes[5]->nodeValue);
		}
		return $clubsArray;
	}

	public function getSeasons(){
		$seasonArray = array();
		$allSeasons = $this->xmlDoc->getElementsByTagName('Season');
		
		foreach($allSeasons as $season){
			$allClubs = $season->getElementsByTagName('Skiers');
			
			foreach($allClubs as $club){
				$allSkiers = $club->getElementsByTagName('Skier');  
				
				foreach($allSkiers as $skier){
					$allEntries = $skier->getElementsByTagName('Log')[0]->getElementsByTagName('Entry');
					$distance = 0;
					
					foreach($allEntries as $entry){
						$distance += $entry->childNodes[5]->nodeValue;
					}
					if($club->hasAttribute('clubId')) $clubId = $club->getAttribute('clubId');
					else $clubId = 0;
					
					$seasonArray[count($seasonArray)] = new Season($season->getAttribute('fallYear'),
						$skier->getAttribute('userName'), $clubId, $distance);
				}
			}
		}
		return $seasonArray;
	}
}
?>