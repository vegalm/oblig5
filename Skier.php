<?php

/*
Klasse for skier. for å lagre innformasjon hentet av skier
*/

class Skier {
	public $userName;
	public $firstName;
	public $lastName;
	public $dateOfBirth;

/*
Konstruktor for klassen
skal hente attributes og lagre de.
*/	
	public function __construct($uName, $fName, $lName, $d)  {  
		$this->userName = $uName;
		$this->firstName = $fName;
		$this->lastName = $lName;
		$this->dateOfBirth = $d;
    } 
}
?>