<?php
include_once('ReadXML.php');
include_once('model.php');
include_once('view.php');

class Controller{
	
	private $xml;
	private $sql;
	
	public function __construct(){
		
		$this->xml = new readXML();
		$this->sql = new SQL();		
		
		$this->sql->importXML($this->xml->getSkiers(), 
			$this->xml->getClubs(), $this->xml->getSeasons());
			
			new View();
	}
}

?>