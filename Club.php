<?php

/*
Klasse for Club. for å lagre innformasjon hentet av skier
*/

class Club {
	public $id;
	public $name;
	public $city;
	public $county;

/*
Konstruktor for klassen
skal hente attributes og lagre de.
*/
	public function __construct($i, $n, $c, $co){  
		$this->id = $i;
		$this->name =$n;
		$this->city =$c;
		$this->county =$co;
    } 
}
?>