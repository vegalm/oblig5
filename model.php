<?php
include_once('Skier.php');
include_once('Club.php');
include_once('Season.php');

class SQL{
	
	private $sql;

	public function __construct(){
		try{
			$this->sql = new PDO('mysql:host=localhost;dbname=oblig5;charset=utf8mb4', 'root');
		}
		catch(PDOException $e){
			echo 'something went wrong with connecting to the database\n';
		}
	}
	
	public function importXML($skiers, $clubs, $seasons){
		$this->updateSkiers($skiers);
		$this->updateClubs($clubs);
		$this->updateSeason($seasons);
	}
	
	public function updateSkiers($allSkiers){
		foreach($allSkiers as $ski){
			$stmt = $this->sql->prepare("INSERT INTO skier (userName, firstName, lastName, dateOfBirth) VALUES(?, ?, ?, ?)");
			$stmt->execute(array($ski->userName, $ski->firstName, $ski->lastName, $ski->dateOfBirth));
		}
	}
	
	public function updateClubs($allClubs){
		foreach($allClubs as $club){
			$stmt = $this->sql->prepare("INSERT INTO clubs (id, name, city, county) VALUES(?, ?, ?, ?)");
			$stmt->execute(array($club->id, $club->name, $club->city, $club->county));
		}
	}
	
	public function updateSeason($allSeasons){
		foreach($allSeasons as $season){
			$stmt = $this->sql->prepare("INSERT INTO seasoncard (fallYear, userName, clubId, distance) VALUES(?, ?, ?, ?)");
			$stmt->execute(array($season->fallYear, $season->userName, $season->club, $season->distance));
		}
	}
}
?>