<?php

/*
Klasse for Season. for å lagre innformasjon hentet av skier
*/

class Season {
	public $fallYear;
	public $userName;
	public $club;
	public $distance;

/*
Konstruktor for klassen
skal hente attributes og lagre de.
*/	
	public function __construct($f, $s, $c, $d)  {  
		$this->fallYear = $f;
		$this->userName = $s;
		$this->club = $c;
		$this->distance = $d;		
    } 
}

?>